# `*` Recherche rapide

`*` en mode commande: permet de faire une recherche sur le mot qui se trouve 
devant ou sous le curseur.   
Possible de se parcourir les différentes occurences en utilisant n 
(occurence suivante) ou N (occurence précédente).

# `q:` Historique de commande

`q:` en mode commande : permet d'ouvrir l'aperçu de l'historique de commande.
Une fois ouvert, il peut être quitté par `:q`.
Il est possible d'éditer une commande ainsi que de les rejouer en appuyant sur 
la touche entrée.

# `<x>s/<1>/<2>/<options>` Rechercher & remplacer

`<x>s/<1>/<2>/<options>` en mode commande permet de chercher 
l'expression régulière `<1>` et de la remplacer par l'expression `<2>`.

`<x>` permet de déterminer la plage de recherche :

* ` ` recherche sur la ligne en cours
* `%` recherche dans tout le fichier
* `12,15` recherche de la ligne 12 à la ligne 15
* `.,$` recherche de la ligne courante à la fin du fichier
* `.,+2` recherche sur deux lignes à partir de la ligne courante 

Les options possibles (cumulables) :

* `g` 0xavier va écrire cette partie de la documentation (todo)
* `c` demande de confirmation avant chaque remplacement
